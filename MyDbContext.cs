﻿using Microsoft.EntityFrameworkCore;

namespace ConsoleApp8
{
    public class MyDbContext:DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Dogs> Dogs { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Server=.; Database=Todo; uid = sa; pwd = 123456; Trusted_Connection = False;");//连接字符串
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //从指定的程序集里面加载IEntityTypeConfiguration内容this.GetType()
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }
    }
}
