﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp8
{
    //作用：按照代码建数据库类，对应那一个表对应
    public class BookConfig:IEntityTypeConfiguration<Book>//接口
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("T_Books");
            builder.Property(b => b.Title).HasMaxLength(50).IsRequired();//Property属性
            builder.Property(b => b.AuthorName).HasMaxLength(20).IsRequired();
        }
    }
}
